app.controller("cotacoesController", function($scope, $http) { 

	$scope.animationsEnabled = true;

	// $scope.path = "http://0.0.0.0:5000/";
	$scope.path = "http://mpbe.pythonanywhere.com/";

	$scope.licitacoes = []
	$scope.produtos = []
	// $scope.licitacao_id = '';
	// $scope.portal_licitacao = '';
	// $scope.models = [];

	// $scope.modalInstanceLicitacao = [];
	// $scope.modalInstanceAlert = [];

	// Função que faz o get de todas as licitacoes a serem cotadas
	$scope.get_licitacoes_a_cotar_web = function(){
		$http.get($scope.path + "get_licitacoes_a_cotar_web").
			success(function(data, status, headers, config) {
				angular.extend($scope.licitacoes, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	// Função que faz o get de todos os produtos a serem cotados
	$scope.get_produtos_a_cotar_web = function(){
		$http.get($scope.path + "get_produtos_a_cotar_web").
			success(function(data, status, headers, config) {
				angular.extend($scope.produtos, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	$scope.enviar_cotacao = function(licitacao, produto){
		console.warn(licitacao);
		console.warn(produto);
	}

	$scope.fill_with_id = function(licitacao, index, id){
		console.warn("oq?");
		licitacao[index] = {};
		licitacao[index].id = id;
	}

	// Chamada para fazer o get de todas as licitações a serem cotadas
	$scope.get_licitacoes_a_cotar_web();
	// Chamada para fazer o get de todas os produtos a serem cotados
	$scope.get_produtos_a_cotar_web();

	var _selected;

	$scope.selected = undefined;
	$scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
	// Any function returning a promise object can be used to load values asynchronously
	$scope.getLocation = function(val) {
		return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
			params: {
				address: val,
				sensor: false
			}
		}).then(function(response){
			return response.data.results.map(function(item){
				return item.formatted_address;
			});
		});
	};

	$scope.ngModelOptionsSelected = function(value) {
		if (arguments.length) {
			_selected = value;
		} else {
			return _selected;
		}
	};

	$scope.modelOptions = {
		debounce: {
			default: 500,
			blur: 250
		},
		getterSetter: true
	};

	$scope.placement = {
		selected: 'bottom-right'
	};

	// $scope.animationsEnabled = true;

	// $scope.openLicitacao = function (size, parentSelector) {
	// 	var parentElem = parentSelector ? 
	// 	angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
	// 	var modalInstance = $uibModal.open({
	// 		animation: $scope.animationsEnabled,
	// 		ariaLabelledBy: 'modal-title',
	// 		ariaDescribedBy: 'modal-body',
	// 		templateUrl: '/html/licitacaoModal.html',
	// 		controller: 'licitacaoModalController',
	// 		size: size,
	// 		appendTo: parentElem,
	// 		resolve: {
	// 			lista_de_documentos: function () {
	// 				return $scope.lista_de_documentos;
	// 			}
	// 		}
	// 	});
	// };
});