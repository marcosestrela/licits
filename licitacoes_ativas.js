app.controller("licitacoesAtivasController", function($uibModal, $log, $document, $scope, $http, $window, $sce) { 

	$scope.animationsEnabled = true;

	// $scope.path = "http://0.0.0.0:5000/";
	$scope.path = "http://mpbe.pythonanywhere.com/";
	
	$scope.licitacoes = [];
	$scope.licitacao = [];
	$scope.licitacao_id = '';
	$scope.portal_licitacao = '';
	$scope.models = [];

	$scope.modalInstanceLicitacao = [];
	$scope.modalInstanceAlert = [];

	// Função que faz o get de todas as dispensas_ativas
	$scope.get_licitacoes_ativas_web = function(){
		$http.get($scope.path + "get_licitacoes_ativas_web").
			success(function(data, status, headers, config) {
				$scope.licitacoes = [];
				angular.extend($scope.licitacoes, data);
				console.warn($scope.licitacoes);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	// Função que faz o get de todas os filtros 
	$scope.get_filtros_web = function(){
		$http.get($scope.path + "get_filtros_web").
			success(function(data, status, headers, config) {
				$scope.lista_de_filtros = [];
				angular.extend($scope.lista_de_filtros, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}
	// Chamada para fazer o get de todas as dispensas_ativas
	$scope.get_licitacoes_ativas_web();
	// Chamada para fazer o get de todas os filtros
	$scope.get_filtros_web();

	// Função que mostra o modal de confirm
	$scope.confirm_modal = function() {
		var modalHtml = `<div class="modal-header">
							<h3 class="modal-title" style="text-align:center;">Esconder licitação</h3>
						</div>
						<div class="modal-body">
							Tem certeza que deseja esconder essa dispensa? Você só conseguirá ver ela novamente clicando na opção "Mostrar todas as dispensas".
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" ng-click="esconder_licitacao_web(licitacao)">
								OK
							</button>
							<button class="btn btn-warning" ng-click="$dismiss()">
								Cancelar
							</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

	// Função que mostra o modal de alert
	$scope.error_modal = function(title, msg) {
		var modalHtml = `<div class="modal-header">
							<h3 class="modal-title" style="text-align:center;">` + title + `</h3>
						</div>
						<div class="modal-body">
							` + msg + `
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" ng-click="$dismiss()">
								OK
							</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

    // Função que esconde uma dispensa que não seja de interesse da listagem
	$scope.esconder_licitacao_web = function(licitacao) {
		var id;
		var method = '';
		if(licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt;
			method = 'esconder_licitacao_comprasnet_federal_web'
		}
		else if(licitacao.licitacoes_comprasnetBA_ativas_codigo_completo_txt){
			id = licitacao.licitacoes_comprasnetBA_ativas_codigo_completo_txt;
			method = 'esconder_licitacao_comprasnetBA_web'
		}
		else if (licitacao.dispensas_ativas_codigo_txt){
			id = licitacao.dispensas_ativas_codigo_txt;
			method = 'esconder_dispensa_ativa_web'
		}
		// Fazer chamada para esconder o licitacao
		$http.post($scope.path + method, id).
			success(function(data, status, headers, config){
				if(data != "success"){
					console.warn('Erro de sistema');
					$scope.error_modal("Erro de sistema", data);
				}
				else {
					$scope.get_licitacoes_ativas_web();
					$scope.modalInstanceAlert.close();
					$scope.modalInstanceLicitacao.close();
				}
			})
			.error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};


    // Função que salva o valor referencial de um produto
	$scope.salvar_valores_referenciais = function(portal) {
		var dados = [];
		var item = {};
		for(i=0; i < $scope.models.length; i++){
			if(angular.isDefined($scope.models[i])){
				if($scope.models[i] != ''){
					var id = $scope.produtos[i].id
					item = {[id]: $scope.models[i]};
					dados.push(item);
				}
			}
		}
		if(dados.length > 0){ // Existem valores a serem enviados
			// Fazer chamada para atualizar os valores referenciais
			var method = '';
			console.warn(portal);
			if(portal == 'Comprasnet Federal'){
				method = 'salvar_valores_referenciais_licitacao_comprasnet_federal_web'
			}
			else if(portal == 'LicitBB'){
				method = 'salvar_valores_referenciais_licitacao_comprasnetBA_web'
			}
			else if(portal == 'ComprasnetBA'){
				method = 'salvar_valores_referenciais_dispensa_ativa_web'
			}
			console.warn($scope.path);
			console.warn(method);
			console.warn(dados);
			$http.post($scope.path + method, dados).
				success(function(data, status, headers, config){
					if(data != "success"){
						console.warn('Erro de sistema');
						$scope.error_modal('Erro de sistema', data);
					}
					else {
						$scope.models = [];
						$scope.modalInstanceLicitacao.close();
						$scope.openLicitacao($scope.licitacao_id, portal);
					}
				})
				.error(function(data, status, headers, config) {
					console.warn('Erro de requisicao');
					console.warn(data);
					$scope.error_modal('Erro de requisição', data);
				})
			;
		}
	};

    // Função que aceita o termo de comprimisso de uma licitação especifica
	$scope.solicitar_cotacao_web = function(licitacao) {
		var id;
		var method = '';
		if(licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt;
			method = 'solicitar_cotacao_licitacao_comprasnet_federal_web'
		}
		else if(licitacao.licitacoes_comprasnetBA_ativas_codigo_completo_txt){
			id = licitacao.licitacoes_comprasnetBA_ativas_codigo_completo_txt;
			method = 'solicitar_cotacao_licitacao_comprasnetBA_web'
		}
		else if (licitacao.dispensas_ativas_codigo_txt){
			id = licitacao.dispensas_ativas_codigo_txt;
			method = 'solicitar_cotacao_dispensa_ativa_web'
		}

		$http.post($scope.path + method, id).
			success(function(data, status, headers, config) {
				if(data == "success"){
					$scope.get_licitacoes_ativas_web();
					$scope.modalInstanceLicitacao.close();
				}
				else {
					console.warn('Erro de sistema');
					$scope.error_modal('Erro de sistema', data);
				}
			}).
			error(function(data, status, headers, config) {
					console.warn('Erro de requisicao');
					$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	$scope.openLicitacao = function (id, portal) {
	console.warn($scope.models);
		var dados = {"id": id, "portal": portal};
		$scope.portal_licitacao = portal;
		$scope.licitacao_id = id;
		$http.post($scope.path + "get_licitacao_web", dados).
			success(function(data, status, headers, config) {
				$scope.licitacao = data[0];

				$http.post($scope.path + "get_produtos_licitacao_web", dados).
					success(function(data, status, headers, config) {
						$scope.produtos = data;
						$scope.modalInstanceLicitacao = $uibModal.open({
							animation: $scope.animationsEnabled,
							templateUrl: '/html/licitacaoModal.html',
							scope: $scope,
							size: 'lg',
						});
					}).
					error(function(data, status, headers, config) {
						console.warn('Erro de sistema');
						console.warn(data);
						$scope.error_modal('Erro de sistema', data);
					})
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	$scope.abrir_edital = function (id, portal) {
		$scope.path = "http://0.0.0.0:5000/";
		var pdfUrl = $scope.path + 'ver_edital/' + id;
		if($scope.portal_licitacao == 'LicitBB'){
			$http.get(pdfUrl).
				success(function(data, status, headers, config){
					$window.open("edital=" + id);
				}).
				error(function(data, status, headers, config){
					console.warn(status);
					if(status == '404'){
						console.warn('Edital não encontrado.');
						$scope.error_modal('Edital não encontrado', 'O edital dessa licitação não foi encontrado no sistema.');
					}
					else{
						console.warn('Erro de sistema');
						$scope.error_modal('Erro de sistema', data);
					}
				})
			;
		}
		else{
			var uasg = $scope.licitacao.licitacoes_ativas_comprasnet_federal_codigo_uasg_txt
			var numprp = $scope.licitacao.licitacoes_ativas_comprasnet_federal_numprp_txt
			var modprp = $scope.licitacao.licitacoes_ativas_comprasnet_federal_modprp_txt
			var url = "https://www.comprasnet.gov.br/ConsultaLicitacoes/Download/Download.asp?";
			var parameters = "coduasg=" + uasg + "&numprp=" + numprp + "&modprp=" + modprp;
			$window.open(url + parameters);		
		}
	};

	$scope.openGerenciador = function(){
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'modalFiltros.html',
			size: 'lg',
			controller: 'licitacoesAtivasController'
		});
	};

});