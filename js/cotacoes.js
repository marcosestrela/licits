app.controller("cotacoesController", function($rootScope, $scope, $http) { 

	$scope.animationsEnabled = true;

	$scope.licitacoes = []
	$scope.produtos = []
	$scope.itens = [];
	// $scope.licitacao_id = '';
	// $scope.portal_licitacao = '';
	// $scope.models = [];

	// $scope.modalInstanceLicitacao = [];
	// $scope.modalInstanceAlert = [];

	// Função que faz o get de todas as licitacoes a serem cotadas
	$scope.get_licitacoes_a_cotar_web = function(){
		$http.get($rootScope.path + "get_licitacoes_a_cotar_web").
			success(function(data, status, headers, config) {
				angular.extend($scope.licitacoes, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	// Função que faz o get de todos os produtos a serem cotados
	$scope.get_produtos_a_cotar_web = function(){
		$http.get($rootScope.path + "get_produtos_a_cotar_web").
			success(function(data, status, headers, config) {
				angular.extend($scope.produtos, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	// Função que salva o valor referencial de um produto
	$scope.salvar_valores_cotacao = function(portal, item) {		
		console.warn(portal);
		console.warn(item);
		console.warn($scope.itens);
			// $scope.itens = [];

		// var dados = [];
		// var item = {};
		// for(i=0; i < $scope.models.length; i++){
		// 	if(angular.isDefined($scope.models[i])){
		// 		if($scope.models[i] != ''){
		// 			var id = $scope.produtos[i].id
		// 			item = {[id]: $scope.models[i]};
		// 			dados.push(item);
		// 		}
		// 	}
		// }
		// if(dados.length > 0){ // Existem valores a serem enviados
		// 	// Fazer chamada para atualizar os valores referenciais
		// 	var method = '';
		// 	console.warn(portal);
		// 	if(portal == 'Comprasnet Federal'){
		// 		method = 'salvar_valores_cotacao_licitacao_comprasnet_federal_web'
		// 	}
		// 	else if(portal == 'LicitBB'){
		// 		method = 'salvar_valores_cotacao_licitacao_comprasnetBA_web'
		// 	}
		// 	else if(portal == 'ComprasnetBA'){
		// 		method = 'salvar_valores_cotacao_dispensa_ativa_web'
		// 	}
		// 	console.warn($rootScope.path);
		// 	console.warn(method);
		// 	console.warn(dados);
		// 	$http.post($rootScope.path + method, dados).
		// 		success(function(data, status, headers, config){
		// 			if(data != "success"){
		// 				console.warn('Erro de sistema');
		// 				$scope.error_modal('Erro de sistema', data);
		// 			}
		// 			else {
		// 				$scope.models = [];
		// 				$scope.modalInstanceLicitacao.close();
		// 				$scope.openLicitacao($scope.licitacao_id, portal);
		// 			}
		// 		})
		// 		.error(function(data, status, headers, config) {
		// 			console.warn('Erro de requisicao');
		// 			console.warn(data);
		// 			$scope.error_modal('Erro de requisição', data);
		// 		})
		// 	;
		// }
	};

    // Função que agenda a participacao em alguma licitação
	$scope.agendar_participacao = function(portal, id){
		var licitacao = {};
		console.warn(portal);
		console.warn(id);
		licitacao.id = id;
		licitacao.portal = portal;
		console.warn(licitacao);
		// if(portal == 'Comprasnet Federal'){
		// 	method = 'agendar_participacao_licitacao_comprasnet_federal_web'
		// }
		// else if(portal == 'LicitBB'){
		// 	method = 'agendar_participacao_licitacao_comprasnetBA_web'
		// }
		// else if(portal == 'ComprasnetBA'){
		// 	method = 'agendar_participacao_dispensa_ativa_web'
		// }

		$http.post($rootScope.path + 'agendar_participacao_web', licitacao).
			success(function(data, status, headers, config) {
				if(data == "success"){
					$scope.get_licitacoes_a_cotar_web();
				}
				else {
					console.warn('Erro de sistema', data);
				}
			}).
			error(function(data, status, headers, config) {
					console.warn('Erro de requisicao', data);
			})
		;
	};

	$scope.enviar_cotacao = function(licitacao, produto){
		console.warn(licitacao);
		console.warn(produto);
	}

	// $scope.fill_with_id = function(licitacao, index, id){
	// 	console.warn("oq?");
	// 	licitacao[index] = {};
	// 	licitacao[index].id = id;
	// }

	// Chamada para fazer o get de todas as licitações a serem cotadas
	$scope.get_licitacoes_a_cotar_web();
	// Chamada para fazer o get de todas os produtos a serem cotados
	$scope.get_produtos_a_cotar_web();

	// var _selected;

	// $scope.selected = undefined;
	// $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
	// // Any function returning a promise object can be used to load values asynchronously
	// $scope.getLocation = function(val) {
	// 	return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
	// 		params: {
	// 			address: val,
	// 			sensor: false
	// 		}
	// 	}).then(function(response){
	// 		return response.data.results.map(function(item){
	// 			return item.formatted_address;
	// 		});
	// 	});
	// };

	// $scope.ngModelOptionsSelected = function(value) {
	// 	if (arguments.length) {
	// 		_selected = value;
	// 	} else {
	// 		return _selected;
	// 	}
	// };

	// $scope.modelOptions = {
	// 	debounce: {
	// 		default: 500,
	// 		blur: 250
	// 	},
	// 	getterSetter: true
	// };

	// $scope.placement = {
	// 	selected: 'bottom-right'
	// };

	// $scope.animationsEnabled = true;

	// $scope.openLicitacao = function (size, parentSelector) {
	// 	var parentElem = parentSelector ? 
	// 	angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
	// 	var modalInstance = $uibModal.open({
	// 		animation: $scope.animationsEnabled,
	// 		ariaLabelledBy: 'modal-title',
	// 		ariaDescribedBy: 'modal-body',
	// 		templateUrl: '/html/licitacaoModal.html',
	// 		controller: 'licitacaoModalController',
	// 		size: size,
	// 		appendTo: parentElem,
	// 		resolve: {
	// 			lista_de_documentos: function () {
	// 				return $scope.lista_de_documentos;
	// 			}
	// 		}
	// 	});
	// };
});