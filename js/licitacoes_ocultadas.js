app.controller("licitacoesOcultadasController", function($rootScope, $uibModal, $log, $document, $scope, $http, $window) { 

    $scope.loading = true; // Show loading image
	$scope.animationsEnabled = true;

	$scope.licitacoes = []
	$scope.licitacao = []
	$scope.licitacao_id = '';
	$scope.portal_licitacao = '';

	$scope.modalInstanceLicitacao = [];
	$scope.modalInstanceAlert = [];

	// Função que faz o get de todas as dispensas_agendadas
	$scope.get_licitacoes_ocultadas_web = function(){
		$http.get($rootScope.path + "get_licitacoes_ocultadas_web").
			success(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
		        $scope.licitacoes = [];		        
				angular.extend($scope.licitacoes, data);
			}).
			error(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
				console.warn('Erro de requisicao');
			})
		;
	}

	// Chamada para fazer o get de todas as dispensas_agendadas
	$scope.get_licitacoes_ocultadas_web();

	// Função que agenda a participacao em alguma licitação
	$scope.recuperar_licitacao = function(portal, id){
		if(portal == 'Comprasnet Federal'){
			method = 'retornar_licitacao_comprasnet_federal_para_ativas_web'
		}
		else if(portal == 'LicitBB'){
			method = 'retornar_licitacao_comprasnetBA_para_ativas_web'
		}
		else if(portal == 'ComprasnetBA'){
			method = 'retornar_dispensa_comprasnetBA_para_ativas_web'
		}
		$http.post($rootScope.path + method, id).
			success(function(data, status, headers, config) {
				if(data == "success"){
					$scope.get_licitacoes_ocultadas_web();
				}
				else {
					console.warn('Erro de sistema', data);
				}
			}).
			error(function(data, status, headers, config) {
					console.warn('Erro de requisicao', data);
			})
		;
	};
});