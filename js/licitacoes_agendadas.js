app.controller("licitacoesAgendadasController", function($rootScope, $uibModal, $log, $document, $scope, $http, $window) { 

    $scope.loading = true; // Show loading image
	$scope.animationsEnabled = true;

	$scope.licitacoes = []
	$scope.licitacao = []
	$scope.licitacao_id = '';
	$scope.portal_licitacao = '';

	$scope.modalInstanceLicitacao = [];
	$scope.modalInstanceAlert = [];

	// Função que faz o get de todas as dispensas_agendadas
	$scope.get_licitacoes_agendadas_web = function(){
		$http.get($rootScope.path + "get_licitacoes_agendadas_web").
			success(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
		        $scope.licitacoes = [];
				angular.extend($scope.licitacoes, data);
			}).
			error(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
				console.warn('Erro de requisicao');
			})
		;
	}

	// Chamada para fazer o get de todas as dispensas_agendadas
	$scope.get_licitacoes_agendadas_web();

	// Função que mostra o modal de confirm
	$scope.error_modal = function(title, msg) {
		var modalHtml = `<div class="modal-header">
							<h3 class="modal-title" style="text-align:center;">` + title + `</h3>
						</div>
						<div class="modal-body">
							Ocorreu um erro na requisição. "` + msg + `
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" ng-click="">
								OK
							</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

	$scope.openLicitacao = function (id, portal) {
		var dados = {"id": id, "portal": portal};
		$scope.portal_licitacao = portal;
		$scope.licitacao_id = id;
		$http.post($rootScope.path + "get_licitacao_web", dados).
			success(function(data, status, headers, config) {
				$scope.licitacao = data[0];

				$http.post($rootScope.path + "get_produtos_licitacao_web", dados).
					success(function(data, status, headers, config) {
						$scope.produtos = data;
						$scope.modalInstanceLicitacao = $uibModal.open({
							animation: $scope.animationsEnabled,
							templateUrl: '/html/licitacaoAgendadaModal.html',
							scope: $scope,
							size: 'lg',
						});
					}).
					error(function(data, status, headers, config) {
						console.warn('Erro de sistema');
						console.warn(data);
						$scope.error_modal('Erro de sistema', data);
					})
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};

    // Função que retorna uma licitação para a área de cotação
	$scope.retornar_para_cotacao = function(licitacao) {
		var id;
		var method = '';
		if(licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt;
			method = 'retornar_para_cotacao_licitacao_comprasnet_federal_web'
		}
		else if(licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt;
			method = 'retornar_para_cotacao_licitacao_comprasnetBA_web'
		}
		else if (licitacao.dispensas_ativas_codigo_txt){
			id = licitacao.dispensas_ativas_codigo_txt;
			method = 'retornar_para_cotacao_dispensa_ativa_web'
		}
		// Fazer chamada para esconder o licitacao
		$http.post($rootScope.path + method, id).
			success(function(data, status, headers, config){
				if(data != "success"){
					console.warn('Erro de sistema');
					$scope.error_modal("Erro de sistema", data);
				}
				else {
					$scope.models = [];
					$scope.get_licitacoes_agendadas_web();
					$scope.modalInstanceLicitacao.close();
				}
			})
			.error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	$scope.abrir_disputa = function(){
		var codigo_dispensa = $scope.licitacao.dispensas_ativas_codigo_txt;
		var url = "https://comprasnet3.ba.gov.br//Fornecedor/LoginDispensa.asp?txtFuncionalidade=&txtNumeroDispensa=" + codigo_dispensa	
		$window.open(url);
	}

	$scope.abrir_edital = function (id, portal){
		if($scope.portal_licitacao == 'LicitBB'){
			var codigo_completo = $scope.licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt;
			var url = "https://comprasnet3.ba.gov.br/edital/" + codigo_completo;
			$window.open(url + ".pdf");
		}
		else{
			var uasg = $scope.licitacao.licitacoes_ativas_comprasnet_federal_codigo_uasg_txt
			var numprp = $scope.licitacao.licitacoes_ativas_comprasnet_federal_numprp_txt
			var modprp = $scope.licitacao.licitacoes_ativas_comprasnet_federal_modprp_txt
			var url = "https://www.comprasnet.gov.br/ConsultaLicitacoes/Download/Download.asp?";
			var parameters = "coduasg=" + uasg + "&numprp=" + numprp + "&modprp=" + modprp;
			$window.open(url + parameters);		
		}
	};
});