app.controller('projecoesController', function ($uibModal, $log, $document, $scope) {

	$scope.lista_de_documentos = [
		'CADASTRO NACIONAL DE PESSOA JURÍDICA',
		'CPF',
		'CADASTRO DE CONTRIBUINTE ESTADUAL',
		'CADASTRO DE CONTRIBUINTE MUNICIPAL',
		'REG. FAZEND. FED E A DIVIDA ATIVA E INSS',
		'REGULARIDADE COM A FAZENDA ESTADUAL',
		'REGULARIDADE COM A FAZENDA MUNICIPAL',
		'BALANCO PATRIMONIAL',
		'CONCORDATA E FALENCIA',
		'ALVARA DE VIGILANCIA SANITARIA',
		'CONTRATO SOCIAL (ULTIMA ALTERACAO)',
		'DECLARACAO DO EMPREGADOR',
		'DECLARACAO DE SUPERVENIENCIA',
		'REGULARIDADE COM O FGTS - CEF',
		'DECLARACAO DE ENQUADRAMENTO',
		'CERTIDAO DE DEBITOS TRABALHISTAS'
	];

	$scope.lista_de_documentos_monitorados = [
		'CADASTRO NACIONAL DE PESSOA JURÍDICA',
		'CPF',
		'CADASTRO DE CONTRIBUINTE ESTADUAL',
		'CADASTRO DE CONTRIBUINTE MUNICIPAL',
		// 'REG. FAZEND. FED E A DIVIDA ATIVA E INSS',
		// 'REGULARIDADE COM A FAZENDA ESTADUAL',
		// 'REGULARIDADE COM A FAZENDA MUNICIPAL',
		// 'BALANCO PATRIMONIAL',
		// 'CONCORDATA E FALENCIA',
		// 'ALVARA DE VIGILANCIA SANITARIA',
		// 'CONTRATO SOCIAL (ULTIMA ALTERACAO)',
		// 'DECLARACAO DO EMPREGADOR',
		'DECLARACAO DE SUPERVENIENCIA',
		'REGULARIDADE COM O FGTS - CEF',
		'DECLARACAO DE ENQUADRAMENTO',
		'CERTIDAO DE DEBITOS TRABALHISTAS'
	];
	// console.warn($scope.lista_de_documentos_monitorados);
	$scope.animationsEnabled = true;

	$scope.openGerenciador = function (size, parentSelector) {
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'gerenciadorModalContent.html',
			controller: 'gerenciadorModalController',
			size: size,
			appendTo: parentElem,
			resolve: {
				lista_de_documentos: function () {
					return $scope.lista_de_documentos;
				}
			}
		});
	};

	$scope.openCaptcha = function (size, parentSelector) {
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'captchaModalContent.html',
			controller: 'captchaModalController',
			size: size,
			appendTo: parentElem,
			resolve: {
				lista_de_documentos: function () {
					return $scope.lista_de_documentos;
				}
			}
		});
	};

	$scope.openBrowse = function (size, parentSelector) {
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'browseModalContent.html',
			controller: 'browseModalController',
			size: size,
			appendTo: parentElem,
			resolve: {
				lista_de_documentos: function () {
					return $scope.lista_de_documentos;
				}
			}
		});
	};
});