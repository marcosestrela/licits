app.controller("licitacoesCotarController", function($rootScope, $uibModal, $scope, $http, $window, $timeout){ 

    $scope.loading = true; // Show loading image
	$scope.animationsEnabled = true;

	$scope.licitacoes = []
	$scope.produtos = []
	$scope.itens = [];
	// $scope.licitacao_id = '';
	// $scope.portal_licitacao = '';
	// $scope.itens[index] = [];

	// $scope.modalInstanceLicitacao = [];
	// $scope.modalInstanceAlert = [];

	// Função que faz o get de todas as licitacoes a serem cotadas
	$scope.get_licitacoes_a_cotar_web = function(){
		$http.get($rootScope.path + "get_licitacoes_a_cotar_web").
			success(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
		        $scope.licitacoes = [];
				angular.extend($scope.licitacoes, data);
			}).
			error(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
				console.warn('Erro de requisicao');
			})
		;
	}

	// Função que faz o get de todos os produtos a serem cotados
	$scope.get_produtos_a_cotar_web = function(){
		$http.get($rootScope.path + "get_produtos_a_cotar_web").
			success(function(data, status, headers, config) {
				angular.extend($scope.produtos, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	// Função que mostra o modal de alert
	$scope.error_modal = function(title, msg) {
		var modalHtml = `<div class="modal-header">
							<h3 class="modal-title" style="text-align:center;">` + title + `</h3>
						</div>
						<div class="modal-body">
							` + msg + `
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" ng-click="$dismiss()">
								OK
							</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

	// Função que salva o valor de cotacao de um produto
	$scope.salvar_valores_cotacao = function(portal, index) {
		console.warn(portal);
		console.warn(index);
		console.warn($scope.itens);
		var dados = [];
		var item = {};
		for(i=0; i < Object.keys($scope.itens[index]).length; i++){
			if(angular.isDefined($scope.itens[index][i].valor_cotado) && angular.isDefined($scope.itens[index][i].marca)){
				dados.push($scope.itens[index][i]);
			}
		}
		console.warn(dados);
		if(dados.length > 0){ // Existem valores a serem enviados
			// Fazer chamada para atualizar os valores referenciais
			var method = '';
			if(portal == 'Comprasnet Federal'){
				method = 'salvar_valores_cotacao_licitacao_comprasnet_federal_web'
			}
			else if(portal == 'LicitBB'){
				method = 'salvar_valores_cotacao_licitacao_comprasnetBA_web'
			}
			else if(portal == 'ComprasnetBA'){
				method = 'salvar_valores_cotacao_dispensa_ativa_web'
			}
			$http.post($rootScope.path + method, dados).
				success(function(data, status, headers, config){
					if(data != "success"){
						console.warn('Erro de sistema', data);
						$scope.error_modal('Erro de sistema', data);
					}
					else {
						$scope.get_licitacoes_a_cotar_web();
						$scope.get_produtos_a_cotar_web();
						$scope.itens = [];
					}
				})
				.error(function(data, status, headers, config) {
					console.warn('Erro de requisicao', data);
					$scope.error_modal('Erro de requisição', data);
				})
			;
		}
	};

	// Função que salva o valor referencial de um produto
	$scope.salvar_valores_referenciais = function(portal, index) {
		console.warn(portal);
		console.warn(index);
		console.warn($scope.itens);
		var dados = [];
		var item = {};
		for(i=0; i < Object.keys($scope.itens[index]).length; i++){
			if(angular.isDefined($scope.itens[index][i].valor_referencial)){
				dados.push({id: $scope.itens[index][i].id, valor_referencial: $scope.itens[index][i].valor_referencial});
			}
		}

		console.warn(dados);
		// dados = [];
		if(dados.length > 0){ // Existem valores a serem enviados
			// Fazer chamada para atualizar os valores referenciais
			var method = '';
			console.warn(portal);
			if(portal == 'Comprasnet Federal'){
				method = 'salvar_valores_referenciais_licitacao_comprasnet_federal_web'
			}
			else if(portal == 'LicitBB'){
				method = 'salvar_valores_referenciais_licitacao_comprasnetBA_web'
			}
			else if(portal == 'ComprasnetBA'){
				method = 'salvar_valores_referenciais_dispensa_ativa_web'
			}
			console.warn($rootScope.path);
			console.warn(method);
			console.warn(dados);
			$http.post($rootScope.path + method, dados).
				success(function(data, status, headers, config){
					if(data != "success"){
						console.warn('Erro de sistema');
						$scope.error_modal('Erro de sistema', data);
					}
					else {
						$scope.get_licitacoes_a_cotar_web();
						$scope.get_produtos_a_cotar_web();
						$scope.itens = [];
					}
				})
				.error(function(data, status, headers, config) {
					console.warn('Erro de requisicao');
					$scope.error_modal('Erro de requisição', data);
				})
			;
		}
	};


	// Função que agenda a participacao em alguma licitação
	$scope.agendar_participacao = function(portal, id){
		console.warn(id)
		if(portal == 'Comprasnet Federal'){
			method = 'agendar_participacao_licitacao_comprasnet_federal_web'
		}
		else if(portal == 'LicitBB'){
			method = 'agendar_participacao_licitacao_comprasnetBA_web'
		}
		else if(portal == 'ComprasnetBA'){
			method = 'agendar_participacao_dispensa_ativa_web'
		}
		$http.post($rootScope.path + method, id).
			success(function(data, status, headers, config) {
				if(data == "success"){
					$scope.get_licitacoes_a_cotar_web();
				}
				else {
					console.warn('Erro de sistema', data);
				}
			}).
			error(function(data, status, headers, config) {
					console.warn('Erro de requisicao', data);
			})
		;
	};

    // Função que esconde uma licitação que não seja de interesse na listagem
	$scope.esconder_licitacao_web = function(portal, id) {
		console.warn(id)
		var method = '';
		if(portal == "Comprasnet Federal"){
			method = 'esconder_licitacao_comprasnet_federal_web'
		}
		else if(portal == "LicitBB"){
			method = 'esconder_licitacao_comprasnetBA_web'
		}
		else if (portal == "ComprasnetBA"){
			method = 'esconder_dispensa_ativa_web'
		}
		// Fazer chamada para esconder o licitacao
		$http.post($rootScope.path + method, id).
			success(function(data, status, headers, config){
				if(data != "success"){
					console.warn('Erro de sistema');
					$scope.error_modal("Erro de sistema", data);
				}
				else {
					$scope.get_licitacoes_a_cotar_web();
				}
			})
			.error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	// Chamada para fazer o get de todas as licitações a serem cotadas
	$scope.get_licitacoes_a_cotar_web();

	// Chamada para fazer o get de todas os produtos a serem cotados
	$scope.get_produtos_a_cotar_web();

	// Função que salva o apontamento de uma licitação
	$scope.atualizar_apontamento = function(index){
		var dados = {"id": $scope.licitacoes[index].id, "apontamento": $scope.licitacoes[index].apontamento};
		var portal = $scope.licitacoes[index].portal;

		// Fazer chamada para atualizar os valores referenciais
		var method = '';
		if(portal == 'Comprasnet Federal'){
			method = 'atualizar_apontamento_licitacao_comprasnet_federal_web'
		}
		else if(portal == 'LicitBB'){
			method = 'atualizar_apontamento_licitacao_comprasnetBA_web'
		}
		else if(portal == 'ComprasnetBA'){
			method = 'atualizar_apontamento_dispensa_ativa_web'
		}
		$http.post($rootScope.path + method, dados).
			success(function(data, status, headers, config){
				if(data != "success"){
					console.warn('Erro de sistema', data);
					$scope.error_modal('Erro de sistema', data);
				}
				else {
					$scope.get_licitacoes_a_cotar_web();
					$scope.get_produtos_a_cotar_web();
					$scope.itens = [];
					$scope.licitacoes[index].flag_apontamento = 1;
					$timeout(function() { $scope.licitacoes[index].flag_apontamento = 0;}, 2000);
				}
			})
			.error(function(data, status, headers, config) {
				console.warn('Erro de requisicao', data);
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	$scope.abrir_edital = function (licitacao) {
		if(licitacao.portal == 'LicitBB'){
			var url = "https://comprasnet3.ba.gov.br/edital/" + licitacao.id;
			$window.open(url + ".pdf");
		}
		else{
			var uasg = licitacao.uasg;
			var numprp = licitacao.numprp;
			var modprp = licitacao.modprp;
			var url = "https://www.comprasnet.gov.br/ConsultaLicitacoes/Download/Download.asp?";
			var parameters = "coduasg=" + uasg + "&numprp=" + numprp + "&modprp=" + modprp;
			$window.open(url + parameters);		
		}
	};
});