var app = angular.module('app', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ngRoute'])
.run(function($rootScope, $location) {

	// Define o path a depender se está rodando no servidor de teste ou no servidor de produção
	$rootScope.baseURL = $location.$$absUrl;
	if($location.$$host == "127.0.0.1"){
		$rootScope.path = "http://0.0.0.0:5000/";
	}
	else if($location.$$host == "licitacoes.pe.hu"){
		$rootScope.path = "http://mpbe.pythonanywhere.com/";
	}
	else if($location.$$host == "www.licitacoes.pe.hu"){
		$rootScope.path = "http://mpbe.pythonanywhere.com/";
	}
})
.config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});
	$routeProvider
	.when('/', {
		templateUrl: '/html/main.html',
	})
	.when('/licitacoes', {
		templateUrl: '/html/licitacoes.html',
	})
	.when('/licitacoes_ativas', {
		templateUrl: '/html/licitacoes_ativas.html',
	})
	.when('/licitacoes_agendadas', {
		templateUrl: '/html/licitacoes_agendadas.html',
	})
	.when('/licitacoes_ocultadas', {
		templateUrl: '/html/licitacoes_ocultadas.html',
	})
	.when('/licitacoes_a_cotar', {
		templateUrl: '/html/licitacoes_a_cotar.html',
	})
	.when('/marcas_vencedoras', {
		templateUrl: '/html/marcas_vencedoras.html',
	})
	.when('/documentacao_SAEB', {
		templateUrl: '/html/documentacao_SAEB.html',
	})
	.when('/gerador_de_documentos', {
		templateUrl: '/html/gerador_de_documentos.html',
	})
	// .when('/licitacoes_em_contratacao', {
	// 	templateUrl: 'html/licitacoes_em_contratacao.html',
	// })
	// .when('/licitacoes_vencidas', {
	// 	templateUrl: 'html/licitacoes_vencidas.html',
	// })
	// .when('/bosta', {
	// 	templateUrl: 'html/bosta.html',
	// })
	.when('/edital=:codigoComposto', {
		templateUrl: '/html/edital.html',
	})
	// .when('/cotacoes', {
	// 	templateUrl: '/html/cotacoes.html',
	// })
	.when('/configuracoes', {
		templateUrl: 'html/configuracoes.html',
	})
	.when('/documentos', {
		templateUrl: 'html/documentos.html',
	})
	.when('/projecoes', {
		templateUrl: 'html/projecoes.html',
	})
	.otherwise({redirectTo: '/'});
})
.controller('mainController', function($scope, $location) {
	console.warn($location.absUrl());
})

// .controller('licitacoesController', function($scope) {
// })
.controller('configuracoesController', function($scope) {
	// $scope.oneAtATime = false;

	// $scope.empresa = { 
	// 	cnpj:"", 
	// 	nome_fantasia: "",
	// 	razao_social: "",
	// 	endereco: "",
	// 	edit: false
	// };

	$scope.empresa = { 
		cnpj:"23.792.647/0001-07",
		nome_fantasia: "Prius Distribuidora",
		razao_social: "Prius Distribuicao e Comercio Atacadista Ltda - ME",
		endereco: "R Benjamim Constant, 160, Andar Terreo, Centro, Mata De Sao Joao, BA, CEP 48280-000, Brasil",
		edit: true
	};

	$scope.responsavel = { 
		nome_completo:"", 
		rg:"",
		cpf:"",
		endereco:"",
		edit: false
	};
	$scope.responsavel.nome_completo = "Testando não";
	$scope.status = {
		isCustomHeaderOpen: false,
		isFirstOpen: true,
		isFirstDisabled: false
	};

	$scope.edit = function(form){
		if(form == "empresa"){
			$scope.empresa.edit = false;
		}
		else if(form == "responsavel"){
			$scope.responsavel.edit = false;
		}
	}
	$scope.salvar = function(form){
		if(form == "empresa"){
			$scope.empresa.edit = true;
		}
		else if(form == "responsavel"){
			$scope.responsavel.edit = true;
		}
	}
})

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

.controller('gerenciadorModalController', function ($uibModalInstance, lista_de_documentos) {
	var $scope = this;
	$scope.lista_de_documentos = lista_de_documentos;
	$scope.ok = function () {
		$uibModalInstance.close($scope.selected.item);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
})
.controller('captchaModalController', function ($uibModalInstance) {
	var $scope = this;
	$scope.ok = function () {
		$uibModalInstance.close($scope.selected.item);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
})
.controller('browseModalController', function ($uibModalInstance) {
	var $scope = this;
	$scope.enviar = function () {
		$uibModalInstance.close($scope.selected.item);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
})
.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}]);