app.controller("editalController", function($rootScope, $uibModal, $scope, $http, $sce, $location) { 
	$scope.id = $location.path().split("=").pop();

	var pdfUrl = $rootScope.path + 'ver_edital/' + $scope.id;

	$http.get(pdfUrl,{responseType:'arraybuffer'})
	.success(function (response) {
		var file = new Blob([(response)], {type: 'application/pdf'});
		var fileURL = URL.createObjectURL(file);
		$scope.content = $sce.trustAsResourceUrl(fileURL);
	});

	// Função que mostra o modal de alert
	$scope.error_modal = function(title, msg) {
		var modalHtml = `<div class="modal-header">
						<h3 class="modal-title" style="text-align:center;">` + title + `</h3>
						</div>
						<div class="modal-body">
						Ocorreu um erro na requisição. "` + msg + `"
						</div>
						<div class="modal-footer">
						<button class="btn btn-primary" ng-click="$dismiss()">
						OK
						</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

});