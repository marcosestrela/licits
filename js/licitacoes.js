app.controller("licitacoesController", function($rootScope, $uibModal, $log, $document, $scope, $http) { 

	$scope.animationsEnabled = true;

	$scope.licitacoes = []
	$scope.licitacao = []

	// Função que faz o get de todas as dispensas_ativas
	$scope.get_licitacoes_web = function(){
		$http.get($rootScope.path + "get_licitacoes_web").
			success(function(data, status, headers, config) {
				angular.extend($scope.licitacoes, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}

	// Chamada para fazer o get de todas as dispensas_ativas
	$scope.get_licitacoes_web();

	$scope.openLicitacao = function (id, portal, parentSelector) {
		var dados = {"id": id, "portal": portal};
		$http.post($rootScope.path + "get_licitacao_web", dados).
			success(function(data, status, headers, config) {
				$scope.licitacao = data[0];

				$http.post($rootScope.path + "get_produtos_licitacao_web", dados).
					success(function(data, status, headers, config) {
						$scope.produtos = data;

						var parentElem = parentSelector ? 
						angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
						var modalInstance = $uibModal.open({
							animation: $scope.animationsEnabled,
							ariaLabelledBy: 'modal-title',
							ariaDescribedBy: 'modal-body',
							templateUrl: '/html/licitacaoModal.html',
							scope: $scope,
							size: 'lg',
							appendTo: parentElem,
						});
					}).
					error(function(data, status, headers, config) {
						console.warn('Erro de requisicao');
					})
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	};
});