app.controller("geradorDeDocumentosController", function($rootScope, $uibModal, $log, $document, $scope, $http, $window) { 

    $scope.loading = true; // Show loading image
	$scope.animationsEnabled = true;

	$scope.documentacao = []
	$scope.licitacao = []
	$scope.licitacao_id = '';
	$scope.portal_licitacao = '';

	$scope.modalInstanceLicitacao = [];
	$scope.modalInstanceAlert = [];

	$scope.itens_counter = 1;
	$scope.documentos_array=[
		{titulo: "Declaração de ciência dos requisitos técnicos", valor: 0}, 
		{titulo: "Declaração de elaboração independente de proposta", valor: 0}, 
		{titulo: "Declaração de fato superveniente", valor: 0}, 
		{titulo: "Declaração de ME", valor: 0}, 
		{titulo: "Declaração de não dissolução, fusão, cisão ou incorporação", valor: 0}, 
		{titulo: "Declaração de não emprego de mão de obra de menor", valor: 0}, 
		{titulo: "Declaração de pleno conhecimento", valor: 0}, 
		{titulo: "Declaração quanto a regularidade fiscal", valor: 0}
	];

	$scope.orgaos_array=[
		{titulo: ""}, 
		{titulo: ""}, 
		{titulo: ""}, 
		{titulo: ""}, 
		{titulo: ""} 
	];

	$scope.itens_array=[
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""},
		{indice: "", descricao: "", marca: "", unidade: "", quantidade: "", preco_unitario: ""}
	];


	// Função que faz o get de todos os documentos da SAEB
	$scope.get_documentacao_SAEB_web = function(){
		$http.get($rootScope.path + "get_documentacao_SAEB_web").
			success(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
		        $scope.documentacao = [];
				angular.extend($scope.documentacao, data);
			}).
			error(function(data, status, headers, config) {
		        $scope.loading = false; // hide loading image on ajax success
				console.warn('Erro de requisicao');
			})
		;
	};

	// Chamada para fazer o get dos documentos da SAEB
	$scope.get_documentacao_SAEB_web();

	$scope.emitirDocumento = function(descricao){
		console.warn(descricao);
        switch (descricao){
			case 'CNPJ/CPF':
				var url = "https://www.receita.fazenda.gov.br/PessoaJuridica/CNPJ/cnpjreva/cnpjreva_solicitacao2.asp"
				$window.open(url);
				break;
			case 'CADASTRO DE CONTRIBUINTE ESTADUAL':
				var url = "http://www.sefaz.ba.gov.br/scripts/cadastro/cadastroBa/consultaBa.asp"
				$window.open(url);
				break;
			case 'CADASTRO DE CONTRIBUINTE MUNICIPAL':
				break;
			case 'REG. FAZEND. FED E A DIVIDA ATIVA E INSS':
				var url = "http://www.receita.fazenda.gov.br/Aplicacoes/ATSPO/Certidao/CndConjuntaInter/InformaNICertidao.asp?Tipo=1"
				$window.open(url);
				break;
			case 'REGULARIDADE COM A FAZENDA ESTADUAL':
				var url = "https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.Aspx?Modulo=CREDITO&Tela=DocEmissaoCertidaoInternet&limparSessao=1&sts_link_externo=2"
				$window.open(url);
				break;
			case 'REGULARIDADE COM A FAZENDA MUNICIPAL':
				var url = "https://matadesaojoao.saatri.com.br/"
				$window.open(url);
				break;
			case 'BALANCO PATRIMONIAL':
				break;
			case 'CONCORDATA E FALENCIA':
				var url = "http://esaj.tjba.jus.br/esaj/portal.do?servico=810000"
				$window.open(url);
				break;
			case 'ALVARA DE VIGILANCIA SANITARIA':
				break;
			case 'CONTRATO SOCIAL (ULTIMA ALTERACAO)':
				break;
			case 'DECLARACAO DO EMPREGADOR':
				break;
			case 'DECLARACAO DE SUPERVENIENCIA':
				break;
			case 'REGULARIDADE COM O FGTS - CEF':
				var url = "https://www.sifge.caixa.gov.br/Cidadao/Crf/FgeCfSCriteriosPesquisa.asp"
				$window.open(url);
				break;
			case 'DECLARACAO DE ENQUADRAMENTO':
				break;
        }
	};
});