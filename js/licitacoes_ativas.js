app.controller("licitacoesAtivasController", function($rootScope, $uibModal, $log, $document, $scope, $http, $window, $sce) {

    $scope.loading = true; // Show loading image
	$scope.animationsEnabled = true;

	$scope.licitacoes = []
	$scope.licitacao = []
	$scope.licitacao_id = '';
	$scope.portal_licitacao = '';
	$scope.models = [];

	$scope.modalInstanceLicitacao = [];
	$scope.modalInstanceAlert = [];

	// Função que faz o get de todas as dispensas_ativas
	$scope.get_licitacoes_ativas_web = function(){
		$http.get($rootScope.path + "get_licitacoes_ativas_web").
			success(function(data, status, headers, config) {
				$scope.licitacoes = [];
				angular.extend($scope.licitacoes, data);
				console.warn($scope.licitacoes);
		        $scope.loading = false; // hide loading image on ajax success
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
		        $scope.loading = false; // hide loading image on ajax error
			})
		;
	}

	// Função que faz o get de todas os filtros 
	$scope.get_filtros_web = function(){
		$http.get($rootScope.path + "get_filtros_web").
			success(function(data, status, headers, config) {
				$scope.lista_de_filtros = [];
				angular.extend($scope.lista_de_filtros, data);
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
			})
		;
	}
	// Chamada para fazer o get de todas as dispensas_ativas
	$scope.get_licitacoes_ativas_web();
	// Chamada para fazer o get de todas os filtros
	// $scope.get_filtros_web();

	// Função que mostra o modal de confirm
	$scope.confirm_modal = function() {
		var modalHtml = `<div class="modal-header">
							<h3 class="modal-title" style="text-align:center;">Ocultar licitação</h3>
						</div>
						<div class="modal-body">
							Tem certeza que deseja ocultar essa licitação? Você só conseguirá ver ela novamente na aba de licitações ocultadas.
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" ng-click="esconder_licitacao_web(licitacao)">
								OK
							</button>
							<button class="btn btn-warning" ng-click="$dismiss()">
								Cancelar
							</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

	// Função que mostra o modal de alert
	$scope.error_modal = function(title, msg) {
		var modalHtml = `<div class="modal-header">
							<h3 class="modal-title" style="text-align:center;">` + title + `</h3>
						</div>
						<div class="modal-body">
							` + msg + `
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" ng-click="$dismiss()">
								OK
							</button>
						</div>`
		$scope.modalInstanceAlert = $uibModal.open({
			animation: $scope.animationsEnabled,
			template: modalHtml,
			scope: $scope,
			size: 'sm'
		});
	};

    // Função que esconde uma dispensa que não seja de interesse da listagem
	$scope.esconder_licitacao_web = function(licitacao) {
		var id;
		var method = '';
		if(licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt;
			method = 'esconder_licitacao_comprasnet_federal_web'
		}
		else if(licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt;
			method = 'esconder_licitacao_comprasnetBA_web'
		}
		else if (licitacao.dispensas_ativas_codigo_txt){
			id = licitacao.dispensas_ativas_codigo_txt;
			method = 'esconder_dispensa_ativa_web'
		}
		// Fazer chamada para esconder o licitacao
		$http.post($rootScope.path + method, id).
			success(function(data, status, headers, config){
				if(data != "success"){
					console.warn('Erro de sistema');
					$scope.error_modal("Erro de sistema", data);
				}
				else {
					$scope.models = [];
					$scope.get_licitacoes_ativas_web();
					$scope.modalInstanceLicitacao.close();
				}
			})
			.error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};


	// Função que salva o valor referencial de um produto
	$scope.salvar_valores_referenciais = function(portal) {
		var dados = [];
		var item = {};
		for(i=0; i < $scope.models.length; i++){
			if(angular.isDefined($scope.models[i])){
				if($scope.models[i] != ''){
					var id = $scope.produtos[i].id
					item = {[id]: $scope.models[i]};
					dados.push(item);
				}
			}
		}
		if(dados.length > 0){ // Existem valores a serem enviados
			// Fazer chamada para atualizar os valores referenciais
			var method = '';
			console.warn(portal);
			if(portal == 'Comprasnet Federal'){
				method = 'salvar_valores_referenciais_licitacao_comprasnet_federal_web'
			}
			else if(portal == 'LicitBB'){
				method = 'salvar_valores_referenciais_licitacao_comprasnetBA_web'
			}
			else if(portal == 'ComprasnetBA'){
				method = 'salvar_valores_referenciais_dispensa_ativa_web'
			}
			console.warn($rootScope.path);
			console.warn(method);
			console.warn(dados);
			$http.post($rootScope.path + method, dados).
				success(function(data, status, headers, config){
					if(data != "success"){
						console.warn('Erro de sistema');
						$scope.error_modal('Erro de sistema', data);
					}
					else {
						$scope.models = [];
						$scope.modalInstanceLicitacao.close();
						$scope.openLicitacao($scope.licitacao_id, portal);
					}
				})
				.error(function(data, status, headers, config) {
					console.warn('Erro de requisicao');
					console.warn(data);
					$scope.error_modal('Erro de requisição', data);
				})
			;
		}
	};

    // Função que aceita o termo de comprimisso de uma licitação especifica
	$scope.solicitar_cotacao_web = function(licitacao) {
		var id;
		var method = '';
		if(licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnet_federal_codigo_completo_txt;
			method = 'solicitar_cotacao_licitacao_comprasnet_federal_web'
		}
		else if(licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt){
			id = licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt;
			method = 'solicitar_cotacao_licitacao_comprasnetBA_web'
		}
		else if (licitacao.dispensas_ativas_codigo_txt){
			id = licitacao.dispensas_ativas_codigo_txt;
			method = 'solicitar_cotacao_dispensa_ativa_web'
		}

		$http.post($rootScope.path + method, id).
			success(function(data, status, headers, config) {
				if(data == "success"){
					$scope.get_licitacoes_ativas_web();
					$scope.models = [];
					$scope.modalInstanceLicitacao.close();
				}
				else {
					console.warn('Erro de sistema');
					$scope.error_modal('Erro de sistema', data);
				}
			}).
			error(function(data, status, headers, config) {
					console.warn('Erro de requisicao');
					$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	$scope.openLicitacao = function (id, portal) {
		console.warn($scope.models);
		var dados = {"id": id, "portal": portal};
		$scope.portal_licitacao = portal;
		$scope.licitacao_id = id;
		$http.post($rootScope.path + "get_licitacao_web", dados).
			success(function(data, status, headers, config) {
				$scope.licitacao = data[0];

				$http.post($rootScope.path + "get_produtos_licitacao_web", dados).
					success(function(data, status, headers, config) {
						$scope.produtos = data;
						$scope.modalInstanceLicitacao = $uibModal.open({
							animation: $scope.animationsEnabled,
							templateUrl: '/html/licitacaoModal.html',
							scope: $scope,
							size: 'lg',
						});
					}).
					error(function(data, status, headers, config) {
						console.warn('Erro de sistema');
						console.warn(data);
						$scope.error_modal('Erro de sistema', data);
					})
			}).
			error(function(data, status, headers, config) {
				console.warn('Erro de requisicao');
				$scope.error_modal('Erro de requisição', data);
			})
		;
	};

	$scope.abrir_edital = function (id, portal) {
		var pdfUrl = $rootScope.path + 'ver_edital/' + id;
		if($scope.portal_licitacao == 'LicitBB'){
			var codigo_completo = $scope.licitacao.licitacoes_ativas_comprasnetBA_codigo_completo_txt;
			console.warn(codigo_completo);
			var url = "https://comprasnet3.ba.gov.br/edital/" + codigo_completo;
			$window.open(url + ".pdf");
		}
		else{
			var uasg = $scope.licitacao.licitacoes_ativas_comprasnet_federal_codigo_uasg_txt
			var numprp = $scope.licitacao.licitacoes_ativas_comprasnet_federal_numprp_txt
			var modprp = $scope.licitacao.licitacoes_ativas_comprasnet_federal_modprp_txt
			var url = "https://www.comprasnet.gov.br/ConsultaLicitacoes/Download/Download.asp?";
			var parameters = "coduasg=" + uasg + "&numprp=" + numprp + "&modprp=" + modprp;
			$window.open(url + parameters);		
		}
	};
});