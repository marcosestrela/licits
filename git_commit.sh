#!/bin/bash

## git_commit.sh - Responsavel por adicionar arquivos, comitar e fazer o push do mesmo ao servidor
## Escrito por: Marcos P. B. Estrela (Salvador - Bahia)
## E-mail: marcos.estrela@hotmail.com


## Exemplo de uso: ./git_commit.sh "Comentario do commit"

## Main

if [ $# -eq 0 ]; then
	echo "Chamada sem argumentos.\n"
	echo "A chamada deve ser './git_commit.sh comentario_do_commit'\n"
else
	echo "---Adicionando arquivos"
	git add *
	echo "---Fazendo commit"
	git commit -m "$1"
	echo "---Fazendo push dos arquivos"
	git push origin master
fi
